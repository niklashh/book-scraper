// ==UserScript==
// @name     download e-book
// @version  1
// @match    https://example.com/*
// @require  https://code.jquery.com/jquery-3.6.0.min.js
// ==/UserScript==

const l = console.log

const loadMDI = () => {
  const link = document.createElement('link')
  link.rel = "stylesheet"
 	link.href = "https://fonts.googleapis.com/icon?family=Material+Icons"
  $('head').append(link)
}
loadMDI()

const loadCustomStyle = () => {
  $('head').append(`<style>
  #downloadButton:hover {
    color: rgb(255, 56, 99);
		cursor: pointer;
  }
  </style>`)
}
loadCustomStyle()

const openNextPage = () => $('.orm-Link-root')[1].click()

const getClassIncludes = (c) => $(`div[class*="${c}"]`)

const getPrevUrl = () => getClassIncludes('prevContainer').find('a').attr('href')
const getNextUrl = () => getClassIncludes('nextContainer').find('a').attr('href')

const getSidebar = () => $('section[data-testid="sidebarWidget"]')[0]

const createDownloadButton = () => {
  const /* mut */ button = document.createElement('div')
	button.innerHTML = '<button id="downloadButton" title="generate pdf" style="width: -moz-available;border: 0px;"><span class="material-icons">download</span></button>'
  
  return button
}

const createPage = async (data) => fetch('http://localhost:7777', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    headers: {
      'Content-Type': 'application/json'
    },
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  })

const slurpTitle = () => $('div[data-datadog-id="ucv-nav-title"]')[0].textContent

const slurpAll = () => $('html').html()

// the classname is loadContainer-... when still loading
const getArticle = () => $('div[class*="readerContainer"]')[0]

const slurpArticle = () => {
  const article = getArticle()
  const inner = article.innerHTML
  return `<!DOCTYPE html><html><body id="book-content">${innerFixImgs}</body></html>`
}

const getUrl = () => window.location.pathname

const arrayBufferToBase64 = (buffer) => btoa(String.fromCharCode.apply(null, new Uint8Array(buffer)));

const slurpImages = () => $('img')

const img2Blob = async (url) => fetch(url).then(r => r.blob())

const convertImage = async (img) => {
  const blob = await img2Blob(img.src)
  const base64 = arrayBufferToBase64(await blob.arrayBuffer())
  
  return {
    path: new URL(img.src).pathname,
    base64,
  }
}

const redirect = url => window.location.pathname = url

const halt = () => l('Received halt!')

const handleCommand = (string) => {
  const [command, ...arguments] = string.split('\n')
  switch (command) {
    case 'redirect':
      redirect(arguments[0])
      break
    case 'halt':
      halt()
      break
    default:
      console.error(`Invalid command ${command}`)
  }
}

var handler = {
  inProgress: false,
  begin: function() {
    handler.inProgress = true
    l('begin downloading')
    $('#downloadButton').hide()
  },
  end: function() {
    handler.inProgress = false
    l('end downloading')
    $('#downloadButton').show()
  },
}

const startDownload = async () => {
  if (handler.inProgress) {
    throw new Error("Download already in progress")
  }
  handler.begin()
  
  try {
    const article = slurpArticle()
    const url = getUrl()
    const title = slurpTitle()
    const prev_url = getPrevUrl()
    const next_url = getNextUrl()
    const images = slurpImages()
    l(`loading ${images.length} images`)
    const assets = await Promise.all(images.map((_, item) => convertImage(item)))
    
    const resp = await createPage({
      article,
      prev_url,
      url,
      next_url,
      title,
      assets,
    })
    const command = await resp.text()
		handleCommand(command)
  } catch(error) {
    console.error(error)
  } finally {
  	handler.end()
  }
}

const waitReady = cb => {
  const title = slurpTitle()
  if (title == null || title.length === 0) {
    setTimeout(() => waitReady(cb), 100)
  } else {
    l("ready")
    setTimeout(cb, 1000)
  }
}

$(() => {
	l("==========================\n\nInjecting download scripts\n\n========================")
//   const button = createDownloadButton()
  
//   sidebar = getSidebar()
//   if (!sidebar) throw new Error("No sidebar found")
  
//   const state = {}
//   button.addEventListener('click', startDownload)
  
//   sidebar.append(button)
  waitReady(startDownload)
})
