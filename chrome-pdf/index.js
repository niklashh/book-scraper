/*
 * Some blithering documentation
 * =============================
 *
 * This command line tool uses puppeteer to read a set of html files
 * and render them as pdfs asynchronously. Warning: hihg memroy usage.
 *
 * It naively expects there to be an html file ../public/prologue.html
 * which is prepended to every html file passed as input.
 *
 * The output files are written in the current directory with the
 * filename of the input + .pdf extension.
 */

const path = require('node:path');
const fs = require('node:fs');
const puppeteer = require('puppeteer');

const seconds = () => {
    const hrtime = process.hrtime();
    return hrtime[0];
}

(async() => {
    let paths = process.argv.slice(2);
    if (paths.length == 0) {
        console.error("Missing document paths to convert");
        process.exit(1);
    }

    const start = seconds();

    const browser = await puppeteer.launch();

    const prologue = fs.readFileSync('../public/prologue.html', 'utf8');

    let finished = 0;
    await Promise.all(paths.map(async (p) => {
        p = path.resolve(process.cwd(), p);
        const filename = path.posix.basename(p);
        const url = `file://${p}`; // TODO url escape
        const output = path.join(process.cwd(), `${filename}.pdf`);
        console.log(`Converting document at ${p} to ${output}`);
        const handle = setInterval(() => {
            // Enterprise-level diagnostic warnings :O~~~~~
            if (finished / paths.length > 0.9) {
                console.log(`Still converting ${p}...`);
            }
        }, 5000)

        const page = await browser.newPage();
        page.on('console', async e => {
            const args = await Promise.all(e.args().map(a => a.jsonValue()));
            console.log(...args);
        });
        
        await page.goto(url, {waitUntil: 'domcontentloaded'});
        const content = await page.content();
        const contentFix = prologue + content.replaceAll('src="/api/v2/epubs', 'src="api/v2/epubs');
        await page.setContent(contentFix, {waitUntil: 'networkidle0'});
        fs.writeFileSync(`final-${filename}`, contentFix);

        await page.pdf({
            path: output,
            format: 'A5',
            printBackground: true,
        });
        clearInterval(handle);
        finished += 1;
        console.log(`Finished ${filename}.pdf (${finished}/${paths.length})`)
    }));

    await browser.close();

    const end = seconds();
    printDiagnostics({start, end, count: paths.length, unit: 's'});
})();

const printDiagnostics = ({start, end, count, unit}) => {
    const delta = end - start;
    console.log(`
Took: ${delta}${unit} total
Mean: ${delta/count}${unit}
`);
}
