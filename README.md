# book-scraper

scrapes online e-books into html to be converted to pdf

## Usage

1. load the greasemonkey script
1. create directory
   > this is the folder where state.toml and all resources will end up in
1. run `book-scraper`
1. refresh the first page of the book
1. profit

Note that `styles.css` required by `prologue.html` is not provided due to leagl reasons. Download the appropriate stylesheets from your web book provider web page.

### Tips

- Merge all your html files along with the prologue and open it to check if all the images and fonts are there.

## PDF-generation options

### Pandoc

- download articles and images with the server
- convert html to pdf with -t context

### HTML parsing

- parse the book into a representation of all elements in the book
- convert the structure to latex
- convert the latex to pdf

### Join together HTML

- inject all articles into one html with custom style
- open the html in browser and export it to pdf

This is a legacy way to convert the HTML files to PDF. Scrape the web book and run

```
ln -s ../public/styles.css styles.css
cp ../public/prologue.html combined.html
../combine.sh >> combined.html
```

### Create a concatenated page in the browser

- no server that downloads the resources
- a buffer is filled with the articles until the last one
- finally the dom is replaced and templated with the buffer

### Puppeteer rendering

The node.js project `chrome-pdf` is provided to render HTML files to PDFs. See `chrome-pdf/index.js` for more docs.

```
npm install
node index.js ../path/to/scraped/*.html
../unitepdfs.sh ../patht/to/scraped/steate.toml
# huge profit
```

### PDF Outline

Using [pdfoutline (from nixpkgs)](https://search.nixos.org/packages?channel=23.05&from=0&size=50&sort=relevance&type=packages&query=pdfoutline) you can add a manually written outline to the final PDF:

```
nix-shell -p fntsample
pdfoutline legally_copied_book.pdf outline.txt outlined.pdf
```

The outline file syntax is specified [here](https://www.systutorials.com/docs/linux/man/1-pdfoutline/)

## Issues

- Doesn't save fonts and css files while scraping!!!
- Bad documentation
- Not agnostic enough
- Doesn't handle server/connection errors
