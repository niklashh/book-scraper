use book_scraper::scrape::Page;
use book_scraper::state::State;
use rocket::serde::json::Json;
use rocket::{launch, post, routes};
use std::fs::{create_dir_all, write};
use std::io::Read;
use std::path::{Path, PathBuf};

fn to_relative_path<T: AsRef<Path>>(path: T) -> PathBuf {
    if let Ok(path) = path.as_ref().strip_prefix("/") {
        path.to_path_buf()
    } else {
        path.as_ref().to_path_buf()
    }
}

enum Action {
    Redirect(String),
    Halt,
}

impl From<Action> for String {
    fn from(val: Action) -> Self {
        use Action::*;
        match val {
            Redirect(s) => format!("redirect\n{}", s),
            Halt => "halt".to_string(),
        }
    }
}

#[post("/", data = "<page>")]
fn create_page(page: Json<Page>) -> String {
    let page = page.into_inner();
    let path = PathBuf::from(PathBuf::from(&page.url).file_name().unwrap());

    println!("Writing {} bytes to {:?}", page.article.len(), path);

    write(path, &page.article).expect("able to write page");

    for asset in &page.assets {
        let decoded = base64::decode(&asset.base64).expect("base64 to be valid");
        let path = to_relative_path(&asset.path)
            .iter()
            .filter(|part| part != &"..") // safety first
            .collect::<PathBuf>();

        let parent = path.parent().expect("path to not be /");
        println!("Creating directory structure {:?}", parent);
        create_dir_all(parent).expect("able to create directories");

        println!("Writing {} bytes to {:?}", decoded.len(), path);
        write(path, decoded).expect("able to write asset");
    }

    let mut state_file = State::open();

    let mut content = String::new();
    state_file
        .read_to_string(&mut content)
        .expect("able to read state file");

    let mut state = if content.is_empty() {
        // empty file, use the default
        // State::write_default(&mut state_file).expect("able to write default state");
        State::default()
    } else {
        // non-empty, try to parse
        toml::from_str(&content).expect("state file to be valid TOML")
    };

    state.add_article(page.into());
    state
        .write_to_file(&mut state_file)
        .expect("able to write state file");

    if let Some(next_url) = state.articles.last().unwrap().next_url.clone() {
        // Action::Halt.into()
        Action::Redirect(next_url).into()
    } else {
        Action::Halt.into()
    }
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![create_page])
}
