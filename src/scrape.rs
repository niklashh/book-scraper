use std::path::PathBuf;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Image {
    pub path: PathBuf,
    pub base64: String,
}

#[derive(Deserialize, Debug)]
pub struct Page {
    pub article: String,
    pub prev_url: Option<String>,
    pub url: String,
    pub next_url: Option<String>,
    pub title: String,
    pub assets: Vec<Image>,
}