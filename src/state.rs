use serde::{Deserialize, Serialize};
use std::fs::{File, OpenOptions};
use std::io;
use std::io::{prelude::*, SeekFrom};

use crate::scrape::Page;

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct State {
    pub articles: Vec<Article>,
}

#[derive(Deserialize, Serialize, Debug, Default, PartialEq)]
pub struct Article {
    pub prev_url: Option<String>,
    pub url: String,
    pub next_url: Option<String>,
    pub title: String,
}

impl State {
    pub fn open() -> File {
        OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open("state.toml")
            .expect("able to open or create state.toml")
    }

    pub fn write_default(file: &mut File) -> io::Result<()> {
        State::default().write_to_file(file)
    }

    pub fn write_to_file(&self, file: &mut File) -> io::Result<()> {
        file.seek(SeekFrom::Start(0)).unwrap();
        file.write_all(toml::to_string(self).unwrap().as_bytes())
    }

    pub fn add_article(&mut self, article: Article) {
        if let Some(old_article) = self.articles.iter_mut().find(|a| a.url == article.url) {
            *old_article = article;
        } else {
            // only push new article if it has a unique url
            self.articles.push(article);
        }
    }
}

impl Into<Article> for Page {
    fn into(self) -> Article {
        Article {
            prev_url: self.prev_url,
            url: self.url,
            next_url: self.next_url,
            title: self.title,
        }
    }
}
