#!/bin/sh

set -ex

[ -z "$1" ] && { echo "Missing path to state.toml!"; exit 1; }

# https://unix.stackexchange.com/questions/387076/passing-multiple-parameters-via-xargs
# The final sh will become the $0
cat "$1" |sed -n 's,^url\s=\s".*/\(.*\)"$,\1.pdf,p' |xargs --verbose -rd'\n' sh -c 'pdfunite "$@" united.pdf' sh
